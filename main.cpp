#include <iostream>

#include <OpenGL/gl.h>
#include <GLUT/glut.h>
#include <cmath>

/*  Poor man's approximation of PI */
#define PI 3.1415926535898

using namespace std;

GLuint loadTexture(const string filename, int &width, int &height);


double dim = 2.0; /* dimension of orthogonal box */
int th = 0;   /* azimuth of view angle */
int ph = 0;   /* elevation of view angle */

void drawTexture();

void init() // Called before main loop to set up the program
{
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);
}

void project() {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    if (0) {
        /* perspective */
//        gluPerspective(fov,asp,dim/4,4*dim);
    } else {
        /* orthogonal projection*/
        glOrtho(-dim, +dim, -dim, +dim, -dim, +dim);
    }

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void windowSpecial(int key, int x, int y) {
    /*  Right arrow key - increase azimuth by 5 degrees */
    if (key == GLUT_KEY_RIGHT) th += 5;
        /*  Left arrow key - decrease azimuth by 5 degrees */
    else if (key == GLUT_KEY_LEFT) th -= 5;
        /*  Up arrow key - increase elevation by 5 degrees */
    else if (key == GLUT_KEY_UP) ph += 5;
        /*  Down arrow key - decrease elevation by 5 degrees */
    else if (key == GLUT_KEY_DOWN) ph -= 5;

    /*  Keep angles to +/-360 degrees */
    th %= 360;
    ph %= 360;

    project();
    glutPostRedisplay();
}


void setEye() {
    if (0) {
//        double Ex = -2 * dim * Sin(th) * Cos(ph);
//        double Ey = +2 * dim * Sin(ph);
//        double Ez = +2 * dim * Cos(th) * Cos(ph);
//        /* camera/eye position, aim of camera lens, up-vector */
//        gluLookAt(Ex, Ey, Ez, 0, 0, 0, 0, Cos(ph), 0);
    }
        /*  Orthogonal - set world orientation */
    else {
        glRotatef(ph, 1, 0, 0);
        glRotatef(th, 0, 1, 0);
    }
}

void drawShape() {

    glBegin(GL_TRIANGLES);
    float numberFloat = 360.0;
    for (int k = 0; k < 360; k += 60) {
//        glColor3f(0.0, 0.0, 1.0);
        glTexCoord2d(0, 1);
        glVertex3f(0, 0, 1);

//        glColor3f(0.0, 1.0, 1.0);
        glTexCoord2d(0.5, 0.5);
        glVertex3f(sin(k / numberFloat * 2 * M_PI), cos(k / numberFloat * 2 * M_PI), 0);

//        glColor3f(1.0, 0.0, 0.0);
        k += 60;
        glTexCoord2d(1, 1);
        glVertex3f(sin(k / numberFloat * 2 * M_PI), cos(k / numberFloat * 2 * M_PI), 0);

        k -= 60;
    }
    glEnd();


    glBegin(GL_POLYGON);
    glColor3f(1.0, 0.0, 0.0);
    for (int i = 0; i < 6; ++i) {
//        glTexCoord2d(i / 6.0, i / 6.0);
        glVertex3d(sin(i / 6.0 * 2 * M_PI), cos(i / 6.0 * 2 * M_PI), 0);
    }
    glEnd();
}

// Called at the start of the program, after a glutPostRedisplay() and during idle
// to display a frame
void display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glLoadIdentity();

    /* setup functions */
    setEye();

    /* draw */
//    drawTexture();
    drawShape();


    /*  Flush and swap */
    glFlush();
    glutSwapBuffers();
}

void drawTexture() {
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//
//    GLuint textures;
//    glGenTextures(1, &textures);
//
    int width = 380;
    int height = 379;

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL);
    glActiveTexture(GL_TEXTURE0);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

//    glBindTexture(GL_TEXTURE_2D, textures);



//    int imageSize = 512 * 512 * 3;
//    unsigned char *data;
//    data = new unsigned char[imageSize];
//    FILE *file = fopen("/Users/ighirici/Documents/HERMAN1.bmp", "rb");
//    if (!file) {
//        printf("Image could not be opened\n");
//        return;
//    }
//    fread(data, 1, imageSize, file);
//    fclose(file);



//    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);

    loadTexture("/Users/ighirici/Documents/brink.png", width, height);

}

// Called every time a window is resized to resize the projection matrix
void reshape(int w, int h) {
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-0.1, 0.1, -float(h) / (10.0 * float(w)), float(h) / (10.0 * float(w)), 0.5, 1000.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

int main(int argc, char **argv) {

    glutInit(&argc, argv); // Initializes glut

    // Sets up a double buffer with RGBA components and a depth component
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGBA);

    // Sets the window size to 512*512 square pixels
    glutInitWindowSize(512, 512);

    // Sets the window position to the upper left
    glutInitWindowPosition(0, 0);

    // Creates a window using internal glut functionality
    glutCreateWindow("Lab 1, Open GL");
    glutSpecialFunc(windowSpecial);


    // passes reshape and display functions to the OpenGL machine for callback
    glutReshapeFunc(reshape);
    glutDisplayFunc(display);
    glutIdleFunc(display);

    drawTexture();


    // Starts the program.
    glutMainLoop();
    return 0;
}